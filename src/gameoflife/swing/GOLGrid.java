package gameoflife.swing;

import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author mjafar
 */
public class GOLGrid extends javax.swing.JPanel {

    protected static final int BLOCK_SIZE = 3;
    protected static final java.awt.Color INACTIVE_COLOR = new java.awt.Color(200, 200, 200);
    protected static final java.awt.Color ACTIVE_COLOR = new java.awt.Color(244, 244, 80);
    private GridBlock[][] cellsArray;
    private GridLayout layout;

    public GOLGrid() {
//        init(20, 20);
    }

    /**
     * Creates new form GOLGrid
     */
    public GOLGrid(int gridWidth, int gridHeight) {
        init(gridHeight, gridWidth);
    }

    public void update(boolean[][] grid) {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j]) {
                    cellsArray[j][i].setAlive();
                } else {
                    cellsArray[j][i].setDead();
                }
            }
        }
    }

    private void init(int gridHeight, int gridWidth) {
        layout = new GridLayout(gridHeight, gridWidth);
        setLayout(layout);
        setBounds(0, 0, gridWidth * BLOCK_SIZE, gridHeight * BLOCK_SIZE);

        cellsArray = new GridBlock[gridWidth][gridHeight];
        for (int j = 0; j < gridHeight; j++) {
            for (int i = 0; i < gridWidth; i++) {
                cellsArray[i][j] = new GridBlock();
                cellsArray[i][j].addMouseListener(new golClickListener(i, j));
                add(cellsArray[i][j]);
            }
        }
    }

    public class golClickListener implements MouseListener {

        int i, j;

        public golClickListener(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
            cellsArray[i][j].toggle();
            gameoflife.GameOfLife.toggle(j, i);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            cellsArray[i][j].mouseEntered();
        }

        @Override
        public void mouseExited(MouseEvent e) {
            cellsArray[i][j].mouseExited();
        }
    }
}
