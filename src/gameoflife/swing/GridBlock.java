package gameoflife.swing;

import java.awt.Color;
import javax.swing.border.LineBorder;

/**
 *
 * @author MohammmadJafar MashhadiEbrahim
 */
public class GridBlock extends javax.swing.JPanel {

    private static final LineBorder border = new javax.swing.border.LineBorder(Color.GRAY, 1);
    private boolean on;

    public GridBlock() {
        super();
//        setBorderPainted(false);
//        setFocusPainted(false);
        setBorder(border);
        setFocusable(false);
//        setBounds(0, 0, GOLGrid.BLOCK_SIZE, GOLGrid.BLOCK_SIZE);
        setDead();
    }

    public final void setAlive() {
        setBackground(GOLGrid.ACTIVE_COLOR);
        on = true;
    }

    public final void setDead() {
        setBackground(GOLGrid.INACTIVE_COLOR);
        on = false;
    }

    public final void toggle() {
        if (on) {
            setDead();
        } else {
            setAlive();
        }
    }

    public final void mouseEntered() {
        setBackground(new Color(getBackground().getRed() - 10, getBackground().getGreen() - 10, getBackground().getBlue() - 10));
    }

    public final void mouseExited() {
        setBackground(new Color(getBackground().getRed() + 10, getBackground().getGreen() + 10, getBackground().getBlue() + 10));
    }
}
