/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gameoflife;

/**
 *
 * @author MohammmadJafar MashhadiEbrahim
 */
public class GameOfLifeCore {

    private boolean[][] grid;

    public GameOfLifeCore(int width, int height) {
        grid = new boolean[width][height];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = false;
            }
        }
    }

    public void toggle(int i, int j) {
        if (isAlive(i, j)) {
            setDead(i, j);
        } else {
            setAlive(i, j);
        }
    }

    public void setAlive(int i, int j) {
        grid[i][j] = true;
    }

    public void setAlive(int[] coordinates) {
        setAlive(coordinates[0], coordinates[1]);
    }

    public void setAlive(int[][] coordinates) {
        for (int[] c : coordinates) {
            setAlive(c);
        }
    }

    public void setDead(int i, int j) {
        grid[i][j] = false;
    }

    public void setDead(int[] coordinates) {
        setDead(coordinates[0], coordinates[1]);
    }

    public void setDead(int[][] coordinates) {
        for (int[] c : coordinates) {
            setDead(c);
        }
    }

    public boolean isAlive(int i, int j) {
        return grid[i][j];
    }

    private int getNeighboarsNumber(int[] coordinates) {
        return getNeighboarsNumber(coordinates[0], coordinates[1]);
    }

    private int getNeighboarsNumber(int x, int y) {
        int count = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }
                try {
                    if (grid[i + x][j + y]) {
                        count++;
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    // We just don't count
                }
            }
        }
        return count;
    }

    public void step() {
        boolean[][] newGrid = new boolean[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                int neighboarsNumber = getNeighboarsNumber(i, j);
                if (grid[i][j] && (neighboarsNumber <= 1 || neighboarsNumber >= 4)) {
                    newGrid[i][j] = false;
                } else if (!grid[i][j] && neighboarsNumber == 3) {
                    newGrid[i][j] = true;
                } else {
                    newGrid[i][j] = grid[i][j];
                }
            }
        }
        grid = newGrid;
        System.gc();
    }

    public boolean[][] getGrid() {
        return grid;
    }
}
