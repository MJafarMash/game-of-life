/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gameoflife;

import gameoflife.swing.GameOfLifeScene;

/**
 *
 * @author mjafar
 */
public class GameOfLife {
    public static final int GRID_WIDTH = 20;
    public static final int GRID_HEIGHT = 16;
    public static GameOfLifeCore golCore;

    static {
        golCore = new GameOfLifeCore(GRID_WIDTH, GRID_HEIGHT);
        golCore.setAlive(new int[][]{{10, 10}, {15, 15}});
    }

    public static void startGraphics() {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GameOfLifeScene().setVisible(true);
            }
        });
    }
    
    public static void toggle(int i, int j) {
        golCore.toggle(i, j);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        startGraphics();
    }
}
